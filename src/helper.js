import axios from 'axios'

const axio = axios.create({
    baseURL: 'http://localhost:7000/api',
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'}
});

export default axio