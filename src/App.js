import React, { Component } from 'react';
import {Button,FormGroup,Label,Input,Table} from  'reactstrap'
import api from './helper';
import './App.css';


class App extends Component {

    constructor(props){
      super(props)
      this.state = {
        dataGuest : [],
        fullName : '',
        email : '',
        phoneNumber : '',
        company : '',
        description : '',
        id: '',
        edit: false
      }
      
    }
    async handleChange(event) {
      this.setState({[event.target.name]: event.target.value});
      
    }
    async handleSubmit(){
        let data = {
          fullName : this.state.fullName,
          email : this.state.email,
          phoneNumber : this.state.phoneNumber,
          company : this.state.company,
          description : this.state.description

        } 
        console.log(data)
       
       
        await api.post('/guest',data)
        this.loadData()
       
    }
   async handleDelete(id){
      
      const hapus = window.confirm("Press a button!");
      console.log(hapus)
      if(hapus){
        await api.delete(`/guest/${id}`)
        this.loadData()
      }
      
      

    }
    async handleEdit(id){
      // alert('edit')
      const guest  = this.state.dataGuest.find(guest=>guest.id===id)
      
        this.setState({
        fullName: guest.fullName,
        email : guest.email,
        phoneNumber : guest.phoneNumber,
        company: guest.company,
        description : guest.description,
        edit: !this.state.edit,
        id: id
      })
     

    }

    async handleEditSave(id){
        await api.put(`/guest/${id}`,{
          fullName: this.state.fullName,
          email : this.state.email,
          phoneNumber : this.state.phoneNumber,
          company: this.state.company,
          description : this.state.description
        })

        this.setState({
          fullName: '',
          email:'',
          phoneNumber: '',
          company: "",
          description: '',
          edit: !this.state.edit,
        })
        this.loadData()
    }


    async loadData(){
      const {data} = await api.get('/guest')
      console.log(data)
      this.setState({dataGuest : data})
    }
    componentDidMount(){
        this.loadData()
    }






  render() {
    console.log(this.state.dataGuest)
    return (
      <div className="container">
            <h1 className="text-center">Buku Tamu</h1>
            <FormGroup>
              <Label for="fullName">Nama Lengkap</Label>
              <Input type="text" name="fullName"  value={this.state.fullName} onChange={(e)=>this.handleChange(e)} placeholder="input nama" />
            </FormGroup>
            <FormGroup>
              <Label for="Email">Email</Label>
              <Input type="email" name="email"  value={this.state.email} onChange={(e)=>this.handleChange(e)} placeholder="input email" />
            </FormGroup>
            <FormGroup>
              <Label for="phoneNumber">Nomor Telepon</Label>
              <Input type="number" name="phoneNumber"  value={this.state.phoneNumber} onChange={(e)=>this.handleChange(e)} placeholder="input nomor telepon" />
            </FormGroup>
            <FormGroup>
              <Label for="company">Perusahaan</Label>
              <Input type="text" name="company"  value={this.state.company} onChange={(e)=>this.handleChange(e)} placeholder="input perusahaan" />
            </FormGroup>
            <FormGroup>
              <Label for="description">Keterangan</Label>
              <Input type="textarea" name="description" value={this.state.description} onChange={(e)=>this.handleChange(e)} placeholder="input keterangan kunjungan"  />
            </FormGroup>
            {this.state.edit ? <Button color="primary" onClick={()=>this.handleEditSave(this.state.id)}>Edit</Button> : <Button color="primary" onClick={()=>this.handleSubmit()}>Simpan</Button>}
            
            

            <Table>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>phoneNumber</th>
                  <th>Perusahaan</th>
                  <th>Keterangan</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataGuest.map(member=>(
                 
                  <tr key={member.id}> 
                    <th scope="row">{member.id}</th>
                    <td>{member.fullName}</td>
                    <td>{member.email}</td>
                    <td>{member.phoneNumber}</td>
                    <td>{member.company}</td>
                    <td>{member.description}</td>
                    <td><Button className="btn-danger" onClick={()=>this.handleDelete(member.id)}>Hapus</Button> ||<Button className="btn-warning" onClick={()=>this.handleEdit(member.id)}>Edit</Button> </td>
                  </tr>
                ))}
              </tbody>
            </Table>
      
      </div>
    );
  }
}

export default App;
